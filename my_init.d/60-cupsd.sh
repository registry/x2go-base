#!/bin/bash

if [[ "$CUPS_ENABLE" -eq "1" ]]; then
	/etc/init.d/cups start

	lpadmin -p Virtual_X2GO_Printer -v cups-x2go:/ -P /usr/share/ppd/cups-x2go/CUPS-X2GO.ppd -L location -E
	lpadmin -d Virtual_X2GO_Printer
fi
