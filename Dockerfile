FROM ubuntu:18.04
MAINTAINER Dmitry K "d.p.karpov@gmail.com"

ENV D_TIME_ZONE Europe/Moscow
ENV D_CODEPAGE UTF-8 
ENV D_LANG ru_RU

# this forces dpkg not to call sync() after package extraction and speeds up install
RUN echo "force-unsafe-io" > /etc/dpkg/dpkg.cfg.d/02apt-speedup
# we don't need and apt cache in a container
RUN echo "Acquire::http {No-Cache=True;};" > /etc/apt/apt.conf.d/no-cache

# Set the env variable DEBIAN_FRONTEND to noninteractive
ENV DEBIAN_FRONTEND noninteractive

#RUN dpkg --add-architecture i386 \
RUN apt-get update \
&& apt-get install -y --no-install-recommends \
	wget \
	software-properties-common \
	sudo \
	apt-utils \
&& apt-add-repository multiverse \
&& add-apt-repository ppa:x2go/stable \
&& apt-get update \
&& echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections \
&& echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections \
&& echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
&& apt-get install -y --no-install-recommends \
	runit cups cups-bsd cups-common \
	sshfs x2goserver x2goserver-xsession x2goserver-printing cups-x2go \
	freeipa-client cracklib-runtime \
	libsss-sudo libpam-sss libnss-sss \
	cabextract p7zip unrar unzip zenity winbind \
	ttf-mscorefonts-installer \
	libpam-ldap nscd \
	libfuse2 \
	dbus \
	tzdata dirmngr \
&& cd /tmp && apt-get download fuse && \
cd /tmp && dpkg-deb -x fuse_* . && \
cd /tmp && dpkg-deb -e fuse_* && \
cd /tmp && rm fuse_*.deb && \
cd /tmp && echo -en '#!/bin/bash\nexit 0\n' > DEBIAN/postinst && \
cd /tmp && dpkg-deb -b . /fuse.deb && \
cd /tmp && dpkg -i /fuse.deb && rm -rf /fuse.deb && \
apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /var/tmp/* && \
rm -rf /tmp/* \
/src

ADD my_init/my_init /sbin/my_init
ADD 2.sh /etc/runit/2
ADD ./service /etc/service
ADD ./my_init.d /etc/my_init.d

#RUN curl -sSL https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks -o /usr/bin/winetricks \
#&& chmod 755 /usr/bin/winetricks

RUN ln -fs /var/run/cups/printcap /etc/printcap && \
chmod 777 /tmp && \
mkdir -p \
	/var/run/dbus \
	/etc/container_environment \
	/etc/my_init.d \
	/var/run/sshd && \
chmod +x \
	/sbin/my_init \
	/etc/runit/2 \
	/etc/service/nscd/run \
	/etc/service/sshd/run \
	/etc/service/sssd/run \
	/etc/service/dbus/run \
	/etc/my_init.d/60-cupsd.sh 

EXPOSE 22

CMD ["/sbin/my_init"]

